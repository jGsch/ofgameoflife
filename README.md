# Game Of Life

Implementation of the [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) in Openframeworks.

![Alt text](imgs/example.png?raw=true "Example")

