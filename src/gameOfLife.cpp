#include "gameOfLife.h"

// highly inspired by https://github.com/croby/conway-of

const int SIZE = 1280;
const int CELLSIZE = 2;
const int UPDATE_INTERVAL = 2;
const int startPosX = 2;
const int startPosY = 2;


//--------------------------------------------------------------
void gameOfLife::setup(){
    
    ofSetWindowShape(SIZE, int(SIZE*0.5625));

    nCell = SIZE/CELLSIZE;

    grid = new cell *[nCell];
	for (int i=0; i<nCell; i++) {
	    grid[i] = new cell[nCell];
		for (int j=0; j<nCell; j++) {
            cell *thisCell = &grid[i][j];
			thisCell->currState = rand() % 2;
			thisCell->nextState = false;	
            thisCell->color = ofColor::black;
        }
    }

    ofBackground(ofColor::white);
    ofSetBackgroundAuto(true);
    ofSetWindowTitle("Game of Life");
    ofSetFrameRate(24);

    iter = 0;
}

int gameOfLife::currState(int col, int row) {
  return (col >= 0 && row >= 0 && col < nCell && row < nCell &&
          grid[col][row].currState == true) ? 1 : 0;
}

int gameOfLife::getNumActiveNeighbors(int colIndex, int rowIndex) {
  int ret = 0;
  
  int prevCol = colIndex-1;
  int nextCol = colIndex+1;
  int prevRow = rowIndex-1;
  int nextRow = rowIndex+1;
  
  ret += currState(prevCol, prevRow);
  ret += currState(prevCol, rowIndex);
  ret += currState(prevCol, nextRow);
  
  ret += currState(colIndex, prevRow);
  ret += currState(colIndex, nextRow);
  
  ret += currState(nextCol, prevRow);
  ret += currState(nextCol, rowIndex);
  ret += currState(nextCol, nextRow);
  
  return ret;
}


//--------------------------------------------------------------
void gameOfLife::update(){
    if (ofGetFrameNum() % UPDATE_INTERVAL == 0) {

		for (int i=0; i<nCell; i++) {
			for (int j=0; j<nCell; j++) {
				cell *thisCell = &grid[i][j];
				thisCell->activeNeighbors = getNumActiveNeighbors(i, j);

				bool currState = thisCell->currState;
				int activeNeighbors = thisCell->activeNeighbors;

				if (currState == true && activeNeighbors < 2) {
					thisCell->nextState = false;
				} 
				else if (currState == true && activeNeighbors > 3) {
					thisCell->nextState = false;
				} 
				else if (currState == true && activeNeighbors > 1 && activeNeighbors < 4) {
					thisCell->nextState = true;
					thisCell->color = ofColor::black;
				} 
				else if (currState == false && activeNeighbors == 3) {
					thisCell->nextState = true;
					thisCell->color = true ? ofColor::green : ofColor::black;
				}
			}
		}

		for (int i=0; i<nCell; i++) {
			for (int j=0; j<nCell; j++) {
				grid[i][j].currState = grid[i][j].nextState;		
			}
		}

        iter = iter + 1;
	}
}

//--------------------------------------------------------------
void gameOfLife::drawShape(int x, int y, int width, int height) {
    ofDrawRectangle(x, y, width, height);
    //ofDrawCircle(x, y, width);
}

//--------------------------------------------------------------
void gameOfLife::draw(){

	for (int i=0; i<nCell; i++) {
	    for (int j=0; j<nCell; j++) {
            cell thisCell = grid[i][j];
            if (thisCell.currState == true) {
                ofSetColor(thisCell.color.r, thisCell.color.g, thisCell.color.b);
                ofFill();				
                drawShape(i*CELLSIZE, j*CELLSIZE, CELLSIZE, CELLSIZE);
                ofNoFill();
            }
		}
    }
    
    ofSetColor(60);
    ofDrawBitmapString("iter: " +  std::to_string(iter), 5, 12);
}

//--------------------------------------------------------------
void gameOfLife::keyPressed(int key){

}

//--------------------------------------------------------------
void gameOfLife::keyReleased(int key){

}

//--------------------------------------------------------------
void gameOfLife::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void gameOfLife::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void gameOfLife::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void gameOfLife::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void gameOfLife::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void gameOfLife::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void gameOfLife::windowResized(int w, int h){

}

//--------------------------------------------------------------
void gameOfLife::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void gameOfLife::dragEvent(ofDragInfo dragInfo){ 

}
